# FounderNest Test

This is the main API for first MVP of FounderNest Test. It will provide necessary data for the interface.

### Documentation
I have added explanations about Architecture and decisions taken in the [wiki](https://gitlab.com/pexegouva/foundernest-test/-/wikis/home).

### Development environment

To make it easy to develop in our local machines, I have added `docker` and `docker-compose` to keep services in container and avoid the need of having ruby, mongo etc in the local machine.

You just need to follow next steps to start the server:

1. First of all, you will need to generate container images by executing `docker-compose build` in the project folder.
2. Once this has finished, you will be able to start all containers, so you just need to execute `docker-compose up`
3. To check that all the containers are up, you can type `docker ps`
4. API will be available at `localhost:3000`

### Services
#### Database

MongoDB is our main DB. As you may know there is no need to run migrations for it. I am using **MongoDB Compass** as database interface, you can download it here -> https://www.mongodb.com/products/compass.

Once inside MongoDB Compass, you can connect to the DB by clicking on connect, and copying the next command -> `mongodb://0.0.0.0:27017/admin?readPreference=primary&appname=MongoDB%20Compass&ssl=false`

Or, if you prefer, you can just go to the DB container, and inside it you will be able to execute commands against mongo cli. More info here -> https://docs.mongodb.com/manual/mongo/

The only thing you will have to take care of about the DB is `indexing`. We have all of our indexes defined in each of the Model, so you just need to run the next command to add them to each DB Collection -> `rake db:mongoid:create_indexes`

#### DB Tasks
I have added a couple of tasks to `populate` and `reset` DB to make it easy to test things.
You just have to enter `api` container and execute the next command to **Populate** DB:
  - `rake RAILS_ENV=development db:populate`
and the next command to reset DB:
  - `rake RAILS_ENV=development db:reset`

#### Test Suite

For the moment there are two steps in the test suite:

1. `lint` for the static analysis supported by [Rubocop](https://github.com/rubocop-hq/rubocop). You will need to execute `rubocop` command to pass it.
2. I am using Rspec to define the tests base. You will need to execute `rspec` to run it.

Note: To execute commands you will need to be inside `api` container, where you will have every Rails and Ruby commands.

At the end of the test process, a `coverage` report is generated. You can check information about it going to `coverage` folder inside the project, and using `index.html` to visualize the report information.

#### API documentation

As a documentation tool for the API, I'm using Postmand and Swagger. In postman, I have defined all of the endpoints to improve manual testing and have the API defined. With Swagger, we have a cool visualization of every endpoint.

I have added a container to keep Swagger UI server. To put the container up you just need to execute `docker-compose up documentation-ui`, to access the server go to `localhost/v1/documentation`. The first time you enter it you will have default documentation, to change the documentation to the API definition, you will need to pick OpenAPI 3.0 definition from [here](https://gitlab.com/pexegouva/foundernest-test/-/snippets/2007887), and generate a `swagger.json` file. Then you just need to copy that file to `swagger/openapi_json` folder and restart containers.