source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.3', '>= 6.0.3.2'
# ODM for MongoDB
# More info -> https://docs.mongodb.com/mongoid/current/
gem 'mongoid', '~> 7.1.2'
# Use Puma as the app server
gem 'puma', '~> 4.1'

# Useful, common monads in idiomatic Ruby
# More info -> https://dry-rb.org/gems/dry-monads/1.3/
gem 'dry-monads', '~> 1.3', '>= 1.3.5'
# A lightning fast JSON:API serializer for Ruby Objects
# More info -> https://github.com/Netflix/fast_jsonapi
gem 'fast_jsonapi'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false
# Implements easy CORS on rails
# More info -> https://github.com/cyu/rack-cors
gem 'rack-cors'

group :development, :test do
  # Step-by-step debugging and stack navigation in Pry
  # More info -> https://github.com/deivid-rodriguez/pry-byebug
  gem 'pry-byebug'
  # Test framework.
  # More info -> https://github.com/rspec/rspec-rails
  gem 'rspec-rails', '~> 4.0.0'
  # A library for setting up Ruby objects as test data
  # More info -> https://github.com/thoughtbot/factory_bot_rails
  gem 'factory_bot_rails'
  # Generate fake Objects (test purpose).
  # More info -> https://github.com/faker-ruby/faker
  gem 'faker'
end

group :development do
  # The Listen gem listens to file modifications and notifies you about the changes
  # More info -> https://github.com/guard/listen
  gem 'listen', '~> 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-commands-rubocop'
  gem 'spring-commands-rspec'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Static analyzer and formatter for Ruby.
  # More info -> https://github.com/rubocop-hq/rubocop
  gem 'rubocop', '~> 0.84.0', require: false
  # Ruby language server
  # More info -> https://github.com/castwide/solargraph
  gem 'solargraph', '~> 0.39.8'
end

group :test do
  # MongoID matchers and macros for Rspec
  # More info -> https://github.com/mongoid/mongoid-rspec
  gem 'mongoid-rspec'
  # Clean strategies for MongoDB
  # More info -> https://github.com/DatabaseCleaner/database_cleaner/tree/v1.8.5/adapters/database_cleaner-mongoid
  gem 'database_cleaner-mongoid'
  # Code coverage for Ruby 1.9+ with a powerful configuration library and
  # automatic merging of coverage across test suites.
  # More info --> https://github.com/colszowka/simplecov
  gem 'simplecov', require: false
  # Symple gem for testing sidekiq with Rspec
  # More info -> https://github.com/philostler/rspec-sidekiq
  gem 'rspec-sidekiq'
end

