namespace :v1, defaults: { format: 'json' } do
  resources :rules, only: [:index]

  resources :companies, only: [:index, :update] do
    get 'analysis_criterias', on: :member, to: 'analysis_criterias#index'
  end
end
