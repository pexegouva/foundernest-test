# frozen_string_literal: true

module Factories
  class CompanySerializerFactory
    include Factories::SerializerFactory

    def initialize(companies:, params:)
      @companies = companies
      @params = params
      @serializer_type = params[:serializer_type]
    end

    def build_serializer
      initialize_data_transfer_object
    end

    private

    attr_reader :companies, :params, :serializer_type

    def initialize_data_transfer_object
      options = init_serializer_options(objects: companies)

      case serializer_type
      when Company::CANONICAL_SERIALIZER
        Success(init_company_serializer(companies, options))
      else
        Failure(Error::ServerError::SerializerTypeNotSelected)
      end
    end

    def init_company_serializer(companies, options)
      response = serializer_api_version(controller_route: params[:controller])
      if response.success?
        serializer = "#{response.value!}::CompanySerializers::CompanySerializer".constantize
        serializer.new(companies, options)
      else
        response
      end
    end
  end
end
