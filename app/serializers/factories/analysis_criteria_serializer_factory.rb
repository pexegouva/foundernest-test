# frozen_string_literal: true

module Factories
  class AnalysisCriteriaSerializerFactory
    include Factories::SerializerFactory

    def initialize(analysis_criterias:, params:)
      @analysis_criterias = analysis_criterias
      @params = params
      @serializer_type = params[:serializer_type]
    end

    def build_serializer
      initialize_data_transfer_object
    end

    private

    attr_reader :analysis_criterias, :params, :serializer_type

    def initialize_data_transfer_object
      options = init_serializer_options(objects: analysis_criterias)

      case serializer_type
      when AnalysisCriteria::CANONICAL_SERIALIZER
        options[:include] = [:rule]
        Success(init_analysis_criteria_serializer(analysis_criterias, options))
      else
        Failure(Error::ServerError::SerializerTypeNotSelected)
      end
    end

    def init_analysis_criteria_serializer(analysis_criterias, options)
      response = serializer_api_version(controller_route: params[:controller])
      if response.success?
        serializer = "#{response.value!}::AnalysisCriteriaSerializers::AnalysisCriteriaSerializer".constantize
        serializer.new(analysis_criterias, options)
      else
        response
      end
    end
  end
end
