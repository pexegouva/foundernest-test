# frozen_string_literal: true

module Factories
  class RuleSerializerFactory
    include Factories::SerializerFactory

    def initialize(rules:, params:)
      @rules = rules
      @params = params
      @serializer_type = params[:serializer_type]
    end

    def build_serializer
      initialize_data_transfer_object
    end

    private

    attr_reader :rules, :params, :serializer_type

    def initialize_data_transfer_object
      options = init_serializer_options(objects: rules)

      case serializer_type
      when Rule::CANONICAL_SERIALIZER
        Success(init_rule_serializer(rules, options))
      else
        Failure(Error::ServerError::SerializerTypeNotSelected)
      end
    end

    def init_rule_serializer(rules, options)
      response = serializer_api_version(controller_route: params[:controller])
      if response.success?
        serializer = "#{response.value!}::RuleSerializers::RuleSerializer".constantize
        serializer.new(rules, options)
      else
        response
      end
    end
  end
end
