# frozen_string_literal: true

module Factories
  module SerializerFactory
    include Dry::Monads[:result, :maybe]

    def build_serializer; end

    def serializer_api_version(controller_route:)
      api_version_regex = /\/?([vV]\d{1})\// # Get V{number} or v{number} from route
      api_version = api_version_regex.match(controller_route)

      capitalize = ->(x) { Maybe(x[1].capitalize) }
      Maybe(api_version).bind(capitalize).to_result(Error::ServerError::IncorrectApiVersion)
    end

    def init_serializer_options(objects:)
      options = {}
      options[:is_collection] = true

      options[:is_collection] = false unless collection_kinds(objects)
      options
    end

    private

    def collection_kinds(objects)
      (objects.class == Array || objects.class == Mongoid::Criteria)
    end
  end
end
