# frozen_string_literal: true

module V1::AnalysisCriteriaSerializers
  class AnalysisCriteriaSerializer
    include FastJsonapi::ObjectSerializer
    extend SerializerHelpers

    set_type :analysis_criteria

    attributes :match_analysis

    attribute :created_at do |analysis_criteria|
      date_at_unix_timestamp(analysis_criteria.created_at)
    end

    attribute :updated_at do |analysis_criteria|
      date_at_unix_timestamp(analysis_criteria.updated_at)
    end

    belongs_to :rule, serializer: V1::RuleSerializers::RuleSerializer
  end
end
