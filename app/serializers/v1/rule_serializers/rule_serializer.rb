# frozen_string_literal: true

module V1::RuleSerializers
  class RuleSerializer
    include FastJsonapi::ObjectSerializer
    extend SerializerHelpers

    set_type :rule

    attributes :definition, :importance

    attribute :created_at do |rule|
      date_at_unix_timestamp(rule.created_at)
    end

    attribute :updated_at do |rule|
      date_at_unix_timestamp(rule.updated_at)
    end
  end
end
