# frozen_string_literal: true

module V1::CompanySerializers
  class CompanySerializer
    include FastJsonapi::ObjectSerializer
    extend SerializerHelpers

    set_type :company

    attributes :name, :site_url, :status, :analysis

    attribute :analysis do |company|
      analysis_data(company)
    end

    attribute :created_at do |company|
      date_at_unix_timestamp(company.created_at)
    end

    attribute :updated_at do |company|
      date_at_unix_timestamp(company.updated_at)
    end

    class << self
      def analysis_data(company)
        return {} unless company.analysis.present?

        {
          matching_score: company.analysis.matching_score,
          warnings: company.analysis.warnings,
          missing_info: company.analysis.missing_information,
          must_haves: company.analysis.must_haves,
          super_nice_to_haves: company.analysis.super_nice_to_haves,
          nice_to_haves: company.analysis.nice_to_haves
        }
      end
    end
  end
end
