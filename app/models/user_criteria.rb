# frozen_string_literal: true

class UserCriteria
  include Mongoid::Document
  include Mongoid::Timestamps

  CANONICAL_SERIALIZER = 'user_criteria_serializer'

  belongs_to :rule
  belongs_to :user

  def identity
    _id.to_s
  end
end
