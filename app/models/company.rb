# frozen_string_literal: true

class Company
  include Mongoid::Document
  include Mongoid::Timestamps

  CANONICAL_SERIALIZER = 'company_serializer'

  WAITING_DECISION = 'waiting_decision'
  FIRST_MEETING = 'first_meeting'
  DISCARDED = 'discarded'
  DECISION_STATES = [WAITING_DECISION, FIRST_MEETING, DISCARDED].freeze

  index({ investor_id: 1 }, { name: 'investor_company', background: true })

  validates :name, length: { minimum: 2, maximum: 120 }

  validates_inclusion_of :status, in: DECISION_STATES

  field :name, type: String
  field :site_url, type: String, default: ''
  field :status, type: String, default: WAITING_DECISION

  embeds_one :analysis

  belongs_to :investor, class_name: 'Investor', dependent: :destroy

  def identity
    _id.to_s
  end
end
