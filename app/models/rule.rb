# frozen_string_literal: true

class Rule
  include Mongoid::Document
  include Mongoid::Timestamps

  CANONICAL_SERIALIZER = 'rule_serializer'

  MUST_HAVE = 1
  SUPER_NICE_TO_HAVE = 2
  NICE_TO_HAVE = 3
  IMPORTANCE_LEVELS = [MUST_HAVE, SUPER_NICE_TO_HAVE, NICE_TO_HAVE].freeze

  index({ user_id: 1 }, { name: 'user_rule', background: true })

  validates :definition, presence: true
  validates :definition, length: { minimum: 10, maximum: 200 }

  validates_inclusion_of :importance, in: IMPORTANCE_LEVELS

  field :definition, type: String
  field :importance, type: Integer, default: MUST_HAVE

  belongs_to :user

  def identity
    _id.to_s
  end
end
