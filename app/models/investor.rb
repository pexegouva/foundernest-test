# frozen_string_literal: true

class Investor < User
  has_many :companies, class_name: 'Company'
end
