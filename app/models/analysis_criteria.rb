# frozen_string_literal: true

class AnalysisCriteria
  include Mongoid::Document
  include Mongoid::Timestamps

  CANONICAL_SERIALIZER = 'analysis_criteria_serializer'

  SUCCESSFUL_MATCH = 'yes'
  INCOMPATIBLE_MATCH = 'no'
  UNKNOWN_MATCH = 'unknown'
  MATCH_ANALYSIS_STATES = [SUCCESSFUL_MATCH, INCOMPATIBLE_MATCH, UNKNOWN_MATCH].freeze

  index({ company_id: 1 }, { name: 'company_analysis_criterias', background: true })

  validates_inclusion_of :match_analysis, in: MATCH_ANALYSIS_STATES

  field :match_analysis, type: String, default: UNKNOWN_MATCH

  belongs_to :company
  belongs_to :rule

  def identity
    _id.to_s
  end
end
