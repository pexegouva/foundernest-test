# frozen_string_literal: true

class Analysis
  include Mongoid::Document
  include Mongoid::Timestamps

  HAVE_CRITERIA = 'have_criteria'
  DO_NOT_HAVE_CRITERIA = 'do_not_have_criteria'
  UNKNOWN_CRITERIA = 'unkonwn_criteria'
  MUST_HAVE_STATES = [HAVE_CRITERIA, DO_NOT_HAVE_CRITERIA, UNKNOWN_CRITERIA].freeze

  validates :matching_score, numericality: { less_than_or_equal_to: 100, greater_than_or_equal_to: 0 }
  validates :warnings, numericality: { less_than_or_equal_to: 100, greater_than_or_equal_to: 0 }
  validates :missing_information, numericality: { less_than_or_equal_to: 100, greater_than_or_equal_to: 0 }
  validates :super_nice_to_haves, numericality: { less_than_or_equal_to: 100, greater_than_or_equal_to: 0 }
  validates :nice_to_haves, numericality: { less_than_or_equal_to: 100, greater_than_or_equal_to: 0 }

  validates_inclusion_of :must_haves, in: MUST_HAVE_STATES

  field :matching_score, type: Integer, default: 0
  field :warnings, type: Integer, default: 0
  field :missing_information, type: Integer, default: 0
  field :must_haves, type: String, default: UNKNOWN_CRITERIA
  field :super_nice_to_haves, type: Integer, default: 0
  field :nice_to_haves, type: Integer, default: 0

  embedded_in :company

  def identity
    _id.to_s
  end
end
