# frozen_string_literal: true

class User
  include Mongoid::Document
  include Mongoid::Timestamps

  validates :email, uniqueness: true

  validates :username, uniqueness: true
  validates :username, length: { minimum: 2, maximum: 60 }

  validates :full_name, presence: true
  validates :full_name, length: { minimum: 2, maximum: 120 }

  field :full_name, type: String
  field :username, type: String
  field :email, type: String, default: ''
end
