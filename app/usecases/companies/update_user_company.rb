# frozen_string_literal: true

module Companies
  class UpdateUserCompany
    include Dry::Monads[:result]

    def initialize(company:)
      @company_command = CompanyServices::Command.new(company: company)
    end

    def perform
      result = update_company
      if result.value?
        Success(result.value!)
      else
        Failure(Error::UnprocessableEntity::MongoidValidation.new(message: result.exception.message))
      end
    end

    private

    attr_reader :company_command

    def update_company
      company_command.update
    end
  end
end
