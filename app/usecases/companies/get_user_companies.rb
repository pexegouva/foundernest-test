# frozen_string_literal: true

module Companies
  class GetUserCompanies
    include Dry::Monads[:result]

    def initialize(user:)
      @user = user
    end

    def perform
      result = find_user_companies
      if result.value?
        Success(result.value!)
      else
        Failure(result.exception)
      end
    end

    private

    attr_reader :user

    def find_user_companies
      CompanyServices::Query.find_by_investor_id(investor_id: user.id)
    end
  end
end
