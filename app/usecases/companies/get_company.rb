# frozen_string_literal: true

module Companies
  class GetCompany
    include Dry::Monads[:result]

    def initialize(id:)
      @id = id
    end

    def perform
      result = find_company
      if result.value?
        Success(result.value!)
      else
        Failure(Error::NotFound::CompanyNotFound)
      end
    end

    private

    attr_reader :id

    def find_company
      CompanyServices::Query.find_by_id(id: id)
    end
  end
end
