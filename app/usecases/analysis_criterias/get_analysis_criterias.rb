# frozen_string_literal: true

module AnalysisCriterias
  class GetAnalysisCriterias
    include Dry::Monads[:result]

    def initialize(company_id:)
      @company_id = company_id
    end

    def perform
      result = find_analysis_criterias
      if result.value?
        Success(result.value!)
      else
        Failure(result.exception)
      end
    end

    private

    attr_reader :company_id

    def find_analysis_criterias
      AnalysisCriteriaServices::Query.find_by_company_id(company_id: company_id)
    end
  end
end
