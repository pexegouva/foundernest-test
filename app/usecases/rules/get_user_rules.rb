# frozen_string_literal: true

module Rules
  class GetUserRules
    include Dry::Monads[:result]

    def initialize(user:)
      @user = user
    end

    def perform
      result = find_user_rules
      if result.value?
        Success(result.value!)
      else
        Failure(result.exception)
      end
    end

    private

    attr_reader :user

    def find_user_rules
      RuleServices::Query.find_by_user_id(user_id: user.id)
    end
  end
end
