# frozen_string_literal: true

module Users
  class GetSessionUser
    include Dry::Monads[:result]

    def initialize; end

    def perform
      result = find_session_user
      if result.value? && !result.value!.nil?
        Success(result.value!)
      else
        Failure(Error::NotFound::UserNotFound)
      end
    end

    private

    def find_session_user
      UserServices::Query.first_user
    end
  end
end
