# frozen_string_literal: true

module UserServices
  class Query
    class << self
      include Dry::Monads[:try]

      def first_user
        Try { User.first }
      end
    end
  end
end
