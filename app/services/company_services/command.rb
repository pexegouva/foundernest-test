# frozen_string_literal: true

module CompanyServices
  class Command
    include Dry::Monads[:try]

    attr_reader :company

    def initialize(company:)
      @company = company
    end

    def update
      Try[Mongoid::Errors::Validations] { company.update! }.bind { Value(company) }
    end
  end
end
