# frozen_string_literal: true

module CompanyServices
  class Query
    class << self
      include Dry::Monads[:try, :maybe]

      def find_by_id(id:)
        id = Maybe(id).value_or('')
        Try[Mongoid::Errors::DocumentNotFound] { Company.find(id) }
      end

      def find_by_investor_id(investor_id:)
        Try { Company.where(investor_id: investor_id) }
      end
    end
  end
end
