# frozen_string_literal: true

module AnalysisCriteriaServices
  class Query
    class << self
      include Dry::Monads[:try, :maybe]

      def find_by_company_id(company_id:)
        Try { AnalysisCriteria.includes(:rule).where(company_id: company_id) }
      end
    end
  end
end
