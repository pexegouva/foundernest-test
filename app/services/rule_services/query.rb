# frozen_string_literal: true

module RuleServices
  class Query
    class << self
      include Dry::Monads[:try]

      def find_by_user_id(user_id:)
        Try { Rule.where(user_id: user_id) }
      end
    end
  end
end
