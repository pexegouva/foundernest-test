# frozen_string_literal: true

class CompanyPolicy
  include Dry::Monads[:maybe]

  attr_reader :current_user, :resource

  def initialize(current_user:, resource:)
    @current_user = current_user
    @resource = resource
  end

  def owner?
    user_is_owner = ->(x) { Maybe(x.investor_id == current_user.id) }
    Maybe(@resource).bind(user_is_owner).value_or(false)
  end
end
