# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Error::ErrorHandler

  attr_reader :current_user

  before_action :authenticate_user

  private

  def authenticate_user
    get_user_use_case = Users::GetSessionUser.new
    @current_user = get_user_use_case.perform.value_or { |err| raise err }
  end
end
