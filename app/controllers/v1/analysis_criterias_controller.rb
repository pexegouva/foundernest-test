# frozen_string_literal: true

module V1
  class AnalysisCriteriasController < ApplicationController
    def index
      analysis_criteria_use_case = AnalysisCriterias::GetAnalysisCriterias.new(company_id: params[:id])
      analysis_criterias = analysis_criteria_use_case.perform.value_or { |err| raise err }

      params[:serializer_type] = AnalysisCriteria::CANONICAL_SERIALIZER
      render json: serialize_analysis_criterias(analysis_criterias, params), status: :ok
    end

    private

    def serialize_analysis_criterias(analysis_criterias, params)
      serializer = Factories::AnalysisCriteriaSerializerFactory.new(
        analysis_criterias: analysis_criterias, params: params
      )
      serializer.build_serializer.value_or { |err| raise err }
    end
  end
end
