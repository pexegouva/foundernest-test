# frozen_string_literal: true

module V1
  class CompaniesController < ApplicationController
    before_action :company_params, only: [:update]
    before_action :init_company, only: [:update]
    before_action :init_company_policy, only: [:update]

    def index
      company_use_case = Companies::GetUserCompanies.new(user: current_user)
      companies = company_use_case.perform.value_or { |err| raise err }

      params[:serializer_type] = Company::CANONICAL_SERIALIZER
      render json: serialize_companies(companies, params), status: :ok
    end

    def update
      raise Error::Forbidden::NotCompanyInvestor unless @company_policy.owner?

      update_company_attributes
      update_company_use_case = Companies::UpdateUserCompany.new(company: @company)
      update_company_use_case.perform.or { |err| raise err }

      render json: {}, status: :no_content
    end

    private

    def serialize_companies(companies, params)
      serializer = Factories::CompanySerializerFactory.new(companies: companies, params: params)
      serializer.build_serializer.value_or { |err| raise err }
    end

    def company_params
      params.require(:company).permit(:status)
    end

    def init_company
      find_company
    end

    def find_company
      result = Companies::GetCompany.new(id: params[:id])
      @company = result.perform.value_or { |err| raise err }
    end

    def update_company_attributes
      @company.status = params[:company][:status]
    end

    def init_company_policy
      @company_policy = CompanyPolicy.new(current_user: current_user, resource: @company)
    end
  end
end
