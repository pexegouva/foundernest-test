# frozen_string_literal: true

module V1
  class RulesController < ApplicationController
    def index
      get_rule_use_case = Rules::GetUserRules.new(user: current_user)
      rules = get_rule_use_case.perform.value_or { |err| raise err }

      params[:serializer_type] = Rule::CANONICAL_SERIALIZER
      render json: serialize_rules(rules, params), status: :ok
    end

    private

    def serialize_rules(rules, params)
      serializer = Factories::RuleSerializerFactory.new(rules: rules, params: params)
      serializer.build_serializer.value_or { |err| raise err }
    end
  end
end
