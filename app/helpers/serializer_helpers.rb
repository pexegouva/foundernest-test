# frozen_string_literal: true

module SerializerHelpers
  def date_at_unix_timestamp(date)
    return 0 unless date.present?

    Integer(date)
  end
end
