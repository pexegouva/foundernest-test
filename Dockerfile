FROM ruby:2.7.1

# Allow utf-8 characters inside Docker container consoles.
ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8

# Install essentials to run ruby in the container.
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

RUN mkdir /founder_main_api
WORKDIR /founder_main_api

COPY Gemfile /founder_main_api/Gemfile
COPY Gemfile.lock /founder_main_api/Gemfile.lock
RUN bundle install --jobs 4 --retry 3

COPY . /founder_main_api

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
