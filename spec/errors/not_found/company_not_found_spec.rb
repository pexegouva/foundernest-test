# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::NotFound::CompanyNotFound, type: :error do
  subject(:not_found) { Error::NotFound::CompanyNotFound }

  describe '.initialize' do
    it 'returns correct json for the error' do
      error = not_found.new
      expect(error.error).to eq :not_found
      expect(error.code).to eq COMPANY_NOT_FOUND_ERROR_CODE
      expect(error.message).to eq 'Company was not found in DB'
      expect(error.status).to eq 404
    end
  end
end
