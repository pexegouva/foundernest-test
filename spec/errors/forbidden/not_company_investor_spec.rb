# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::Forbidden::NotCompanyInvestor, type: :error do
  subject(:forbidden_error) { Error::Forbidden::NotCompanyInvestor }

  describe '.initialize' do
    it 'returns correct json for the error' do
      error = forbidden_error.new
      expect(error.error).to eq :forbidden
      expect(error.code).to eq NOT_COMPANY_INVESTOR_ERROR_CODE
      expect(error.message).to eq 'User is not the investor of the given Company'
      expect(error.status).to eq 403
    end
  end
end
