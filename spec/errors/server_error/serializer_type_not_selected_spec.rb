# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::ServerError::SerializerTypeNotSelected, type: :error do
  subject(:server_error) { Error::ServerError::SerializerTypeNotSelected }

  describe '.initialize' do
    it 'returns correct json for the error' do
      error = server_error.new
      expect(error.error).to eq :internal_server_error
      expect(error.code).to eq SERIALIZER_TYPE_NOT_SELECTED_ERROR_CODE
      expect(error.message).to eq 'Serializer type not selected, use existing one'
      expect(error.status).to eq 500
    end
  end
end
