# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::ServerError::IncorrectApiVersion, type: :error do
  subject(:server_error) { Error::ServerError::IncorrectApiVersion }

  describe '.initialize' do
    it 'returns correct json for the error' do
      error = server_error.new
      expect(error.error).to eq :internal_server_error
      expect(error.code).to eq INCORRECT_API_VERSION_ERROR_CODE
      expect(error.message).to eq 'There is no version in the given route'
      expect(error.status).to eq 500
    end
  end
end
