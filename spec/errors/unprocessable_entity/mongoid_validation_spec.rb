# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::UnprocessableEntity::MongoidValidation, type: :error do
  subject(:server_error) { Error::UnprocessableEntity::MongoidValidation }

  describe '.initialize' do
    context 'when no message as params' do
      it 'returns correct json with the error' do
        error = server_error.new
        expect(error.error).to eq :unprocessable_entity
        expect(error.code).to eq MONGOID_VALIDATION_ERROR_CODE
        expect(error.message).to eq 'MongoID validation error'
        expect(error.status).to eq 422
      end
    end

    context 'when message in params' do
      it 'returns correct json with the given message' do
        error = server_error.new(message: 'fake_message')
        expect(error.error).to eq :unprocessable_entity
        expect(error.code).to eq MONGOID_VALIDATION_ERROR_CODE
        expect(error.message).to eq 'fake_message'
        expect(error.status).to eq 422
      end
    end
  end
end
