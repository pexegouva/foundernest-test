# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SerializerHelpers, type: :helpers do
  subject(:dummy_class) { Class.new { extend SerializerHelpers } }

  let(:date_timestamp) { 946684800 }
  let(:date) { Time.at(date_timestamp) }

  describe '#date_at_unix_timestamp' do
    context 'when date present' do
      it 'returns date unix formatted' do
        expect(dummy_class.date_at_unix_timestamp(date)).to eq date_timestamp
      end
    end

    context 'when date not present' do
      it 'returns 0' do
        expect(dummy_class.date_at_unix_timestamp(nil)).to eq 0
      end
    end
  end
end
