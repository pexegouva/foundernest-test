# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  describe '#authenticate_user' do
    context 'when there is no session user' do
      it 'raises Error::NotFound::UserNotFound' do
        expect {
          controller.send(:authenticate_user)
        }.to raise_error(Error::NotFound::UserNotFound)
      end
    end

    context 'when there is session user' do
      let!(:user) { create(:user) }

      it 'initializes current_user instance variable' do
        controller.send(:authenticate_user)
        expect(controller.current_user.id).to eq User.first.id
      end
    end
  end
end
