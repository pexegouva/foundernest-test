# frozen_string_literal: true

module ParsingHelpers
  def json_body
    JSON.parse(response.body, allow_nan: true)
  end
end
