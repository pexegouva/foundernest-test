# frozen_string_literal: true

require 'database_cleaner-mongoid'

RSpec.configure do |config|
  config.before(:each) do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.clean
  end
end
