# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Company, type: :model do
  it { is_expected.to be_mongoid_document }
  it { is_expected.to have_timestamps }

  it do
    is_expected.to have_index_for(investor_id: 1).with_options(background: true)
  end

  it do
    is_expected.to have_field(:name).of_type(String)
    is_expected.to have_field(:site_url).of_type(String).with_default_value_of('')
  end

  it do
    is_expected.to belong_to(:investor).of_type(Investor)
    is_expected.to embed_one(:analysis)
  end

  describe '#name' do
    it { is_expected.to validate_length_of(:name).with_minimum(2).with_maximum(120) }
  end

  describe '#status' do
    it { is_expected.to validate_inclusion_of(:status).to_allow(%w[waiting_decision first_meeting discarded]) }
  end

  describe '#identity' do
    let(:company) { build(:company, _id: BSON::ObjectId('5ecbed4799ba6500165912e1')) }

    it 'returns _id as string' do
      expect(company.identity).to eq '5ecbed4799ba6500165912e1'
    end
  end
end
