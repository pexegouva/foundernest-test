# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Rule, type: :model do
  it { is_expected.to be_mongoid_document }
  it { is_expected.to have_timestamps }

  it do
    is_expected.to have_index_for(user_id: 1).with_options(background: true)
  end

  it do
    is_expected.to have_field(:definition).of_type(String)
    is_expected.to have_field(:importance).of_type(Integer)
  end

  it do
    is_expected.to belong_to(:user)
  end

  describe '#definition' do
    it { is_expected.to validate_presence_of(:definition) }
    it { is_expected.to validate_length_of(:definition).with_minimum(10).with_maximum(200) }
  end

  describe '#importance' do
    it { is_expected.to validate_inclusion_of(:importance).to_allow([1, 2, 3]) }
  end

  describe '#identity' do
    let(:rule) { build(:rule, _id: BSON::ObjectId('5ecbed4799ba6500165912e1')) }

    it 'returns _id as string' do
      expect(rule.identity).to eq '5ecbed4799ba6500165912e1'
    end
  end
end
