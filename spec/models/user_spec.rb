# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to be_mongoid_document }
  it { is_expected.to have_timestamps }

  it do
    is_expected.to have_field(:email).of_type(String)
    is_expected.to have_field(:full_name).of_type(String)
    is_expected.to have_field(:username).of_type(String)
  end

  describe '#email' do
    it { is_expected.to validate_uniqueness_of(:email) }
  end

  describe '#username' do
    it { is_expected.to validate_uniqueness_of(:username) }
    it { is_expected.to validate_length_of(:username).with_minimum(2).with_maximum(60) }
  end

  describe '#full_name' do
    it { is_expected.to validate_presence_of(:full_name) }
    it { is_expected.to validate_length_of(:full_name).with_minimum(2).with_maximum(120) }
  end
end
