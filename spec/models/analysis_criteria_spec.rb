# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AnalysisCriteria, type: :model do
  it { is_expected.to be_mongoid_document }
  it { is_expected.to have_timestamps }

  it do
    is_expected.to have_index_for(company_id: 1).with_options(background: true)
  end

  it do
    is_expected.to have_field(:match_analysis).of_type(String)
  end

  it do
    is_expected.to belong_to(:company)
    is_expected.to belong_to(:rule)
  end

  describe '#match_analysis' do
    it { is_expected.to validate_inclusion_of(:match_analysis).to_allow(%w[yes no unknown]) }
  end

  describe '#identity' do
    let(:analysis_criteria) { build(:analysis_criteria, _id: BSON::ObjectId('5ecbed4799ba6500165912e1')) }

    it 'returns _id as string' do
      expect(analysis_criteria.identity).to eq '5ecbed4799ba6500165912e1'
    end
  end
end
