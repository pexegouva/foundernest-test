# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Analysis, type: :model do
  it { is_expected.to be_mongoid_document }
  it { is_expected.to have_timestamps }

  it do
    is_expected.to have_field(:matching_score).of_type(Integer)
    is_expected.to have_field(:warnings).of_type(Integer)
    is_expected.to have_field(:missing_information).of_type(Integer)
    is_expected.to have_field(:super_nice_to_haves).of_type(Integer)
    is_expected.to have_field(:nice_to_haves).of_type(Integer)
    is_expected.to have_field(:must_haves).of_type(String).with_default_value_of(Analysis::UNKNOWN_CRITERIA)
  end

  it do
    is_expected.to be_embedded_in(:company)
  end

  describe '#matching_score' do
    it {
      is_expected.to(
        validate_numericality_of(:matching_score).greater_than_or_equal_to(0).less_than_or_equal_to(100)
      )
    }
  end

  describe '#warnings' do
    it {
      is_expected.to(
        validate_numericality_of(:warnings).greater_than_or_equal_to(0).less_than_or_equal_to(100)
      )
    }
  end

  describe '#missing_information' do
    it {
      is_expected.to(
        validate_numericality_of(:missing_information).greater_than_or_equal_to(0).less_than_or_equal_to(100)
      )
    }
  end

  describe '#nice_to_haves' do
    it {
      is_expected.to(
        validate_numericality_of(:nice_to_haves).greater_than_or_equal_to(0).less_than_or_equal_to(100)
      )
    }
  end

  describe '#super_nice_to_haves' do
    it {
      is_expected.to(
        validate_numericality_of(:super_nice_to_haves).greater_than_or_equal_to(0).less_than_or_equal_to(100)
      )
    }
  end

  describe '#must_haves' do
    it {
      is_expected.to(
        validate_inclusion_of(:must_haves).to_allow(%w[have_criteria do_not_have_criteria unkonwn_criteria])
      )
    }
  end

  describe '#identity' do
    let(:analysis) { build(:analysis, _id: BSON::ObjectId('5ecbed4799ba6500165912e1')) }

    it 'returns _id as string' do
      expect(analysis.identity).to eq '5ecbed4799ba6500165912e1'
    end
  end
end
