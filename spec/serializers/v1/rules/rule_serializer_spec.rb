# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::RuleSerializers::RuleSerializer, type: :serializer do
  subject(:serializer) { V1::RuleSerializers::RuleSerializer }

  let(:date_timestamp) { 946684800 }
  let(:rule) { build(:rule_with_user, created_at: date_timestamp) }

  describe 'on initialization' do
    it 'returns correct serialized data' do
      serialized_object = serializer.new(rule, {}).serializable_hash[:data]
      expect(serialized_object[:type]).to eq :rule
      expect(serialized_object[:id]).to eq rule.identity

      serialized_attributes = serialized_object[:attributes]
      expect(serialized_attributes[:definition]).to eq rule.definition
      expect(serialized_attributes[:importance]).to eq rule.importance
      expect(serialized_attributes[:created_at]).to eq date_timestamp
      expect(serialized_attributes[:updated_at]).to eq 0
    end
  end
end
