# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::AnalysisCriteriaSerializers::AnalysisCriteriaSerializer, type: :serializer do
  subject(:serializer) { V1::AnalysisCriteriaSerializers::AnalysisCriteriaSerializer }

  let(:date_timestamp) { 946684800 }
  let(:analysis_criteria) { build(:analysis_criteria_with_rule, created_at: date_timestamp) }

  describe 'on initialization' do
    it 'returns correct serialized data' do
      serialized_object = serializer.new(analysis_criteria, { include: [:rule] }).serializable_hash[:data]
      expect(serialized_object[:type]).to eq :analysis_criteria
      expect(serialized_object[:id]).to eq analysis_criteria.identity

      serialized_attributes = serialized_object[:attributes]
      expect(serialized_attributes[:match_analysis]).to eq 'unknown'
      expect(serialized_attributes[:created_at]).to eq date_timestamp
      expect(serialized_attributes[:updated_at]).to eq 0

      rule_relatinship = serialized_object[:relationships][:rule][:data]
      expect(rule_relatinship[:id]).to eq analysis_criteria.rule.identity
      expect(rule_relatinship[:type]).to eq :rule
    end

    context 'and it includes rule in options' do
      let(:options) { { include: [:rule] } }

      it 'returns included rule information' do
        included_rule = serializer.new(analysis_criteria, options).serializable_hash[:included][0]
        expect(included_rule[:id]).to eq analysis_criteria.rule.identity
        expect(included_rule[:type]).to eq :rule

        rule_attributes = included_rule[:attributes]
        expect(rule_attributes[:definition]).to eq analysis_criteria.rule.definition
        expect(rule_attributes[:importance]).to eq analysis_criteria.rule.importance
      end
    end
  end
end
