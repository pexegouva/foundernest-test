# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::CompanySerializers::CompanySerializer, type: :serializer do
  subject(:serializer) { V1::CompanySerializers::CompanySerializer }

  let(:date_timestamp) { 946684800 }
  let(:company) { build(:company, created_at: date_timestamp) }
  let(:analysis) { build(:analysis) }

  describe 'on initialization' do
    before do
      company.analysis = analysis
    end

    it 'returns correct serialized data' do
      serialized_object = serializer.new(company, {}).serializable_hash[:data]
      expect(serialized_object[:type]).to eq :company
      expect(serialized_object[:id]).to eq company.identity

      serialized_attributes = serialized_object[:attributes]
      expect(serialized_attributes[:name]).to eq company.name
      expect(serialized_attributes[:site_url]).to eq company.site_url
      expect(serialized_attributes[:status]).to eq company.status
      expect(serialized_attributes[:created_at]).to eq date_timestamp
      expect(serialized_attributes[:updated_at]).to eq 0

      analysis = serialized_attributes[:analysis]
      expect(analysis[:matching_score]).to eq company.analysis.matching_score
      expect(analysis[:warnings]).to eq company.analysis.warnings
      expect(analysis[:missing_info]).to eq company.analysis.missing_information
      expect(analysis[:must_haves]).to eq company.analysis.must_haves
      expect(analysis[:super_nice_to_haves]).to eq company.analysis.super_nice_to_haves
      expect(analysis[:nice_to_haves]).to eq company.analysis.nice_to_haves
    end
  end
end
