# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Factories::SerializerFactory, type: :factory do
  subject(:dummy_class) { Class.new { extend Factories::SerializerFactory } }

  describe '#serializer_api_version' do
    let(:factory_builder_instance) { factory_builder.new }

    context 'when valid route' do
      let(:valid_route) { 'v1/auth/sign_up' }

      it 'returns Success with version' do
        response = dummy_class.serializer_api_version(controller_route: valid_route)
        expect(response).to eq Success('V1')
      end
    end

    context 'when no version in the route' do
      let(:invalid_route) { '/auth/sign_up' }

      it 'returns Failure with error' do
        response = dummy_class.serializer_api_version(controller_route: invalid_route)
        expect(response).to eq Failure(Error::ServerError::IncorrectApiVersion)
      end
    end
  end

  describe '#init_serializers_options' do
    let(:collection_options) { { is_collection: true } }

    context 'when Array' do
      let(:fake_objects) { %w[fake fake] }

      it 'returns is_collection true in options' do
        response = dummy_class.init_serializer_options(objects: fake_objects)
        expect(response).to eq collection_options
      end
    end

    context 'when MongoID::Criteria' do
      let(:fake_objects) { Mongoid::Criteria.new(User) }

      it 'returns is_collection true in options' do
        response = dummy_class.init_serializer_options(objects: fake_objects)
        expect(response).to eq collection_options
      end
    end

    context 'when not Array nor Mongoid::Criteria' do
      let(:fake_object) { 'fake_object' }
      let(:options) { { is_collection: false } }

      it 'returns is_collection false in options' do
        response = dummy_class.init_serializer_options(objects: fake_object)
        expect(response).to eq options
      end
    end
  end
end
