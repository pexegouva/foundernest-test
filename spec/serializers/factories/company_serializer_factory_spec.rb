# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Factories::CompanySerializerFactory, type: :factory do
  subject(:serializer_factory) { Factories::CompanySerializerFactory }

  describe '#build_serializer' do
    let(:companies) { build_list(:company, 2) }

    context 'when incorrect serializer type' do
      let(:params) { {} }
      let(:serializer_factory_instance) { serializer_factory.new(companies: companies, params: params) }

      it 'returns Failure SerializerTypeNotSelectedError' do
        response = serializer_factory_instance.build_serializer
        expect(response).to eq Failure(Error::ServerError::SerializerTypeNotSelected)
      end
    end

    context 'when serializer CANONICAL_SERIALIZER type defined and correct version route' do
      let(:params) { { controller: 'v1/companies', serializer_type: Company::CANONICAL_SERIALIZER } }
      let(:serializer_factory_instance) { serializer_factory.new(companies: companies, params: params) }
      let(:options) { { is_collection: true } }
      let(:serializer) { V1::CompanySerializers::CompanySerializer }

      it 'builds correct serializer' do
        expect(serializer).to receive(:new).with(companies, options)
        serializer_factory_instance.build_serializer
      end

      context 'and incorrect version route' do
        let(:params) { { controller: '/companies', serializer_type: Company::CANONICAL_SERIALIZER } }
        let(:serializer_factory_instance) { serializer_factory.new(companies: companies, params: params) }

        it 'returns Failure' do
          expect(serializer_factory_instance).to receive(:serializer_api_version).with(
            controller_route: '/companies'
          ).and_return(Failure(Error::ServerError::IncorrectApiVersion))
          serializer_factory_instance.build_serializer
        end
      end
    end
  end
end
