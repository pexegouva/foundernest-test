# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Factories::AnalysisCriteriaSerializerFactory, type: :factory do
  subject(:serializer_factory) { Factories::AnalysisCriteriaSerializerFactory }

  describe '#build_serializer' do
    let(:analysis_criterias) { build_list(:analysis_criteria, 2) }

    context 'when incorrect serializer type' do
      let(:params) { {} }
      let(:serializer_factory_instance) do
        serializer_factory.new(analysis_criterias: analysis_criterias, params: params)
      end

      it 'returns Failure SerializerTypeNotSelectedError' do
        response = serializer_factory_instance.build_serializer
        expect(response).to eq Failure(Error::ServerError::SerializerTypeNotSelected)
      end
    end

    context 'when serializer CANONICAL_SERIALIZER type defined and correct version route' do
      let(:params) { { controller: 'v1/analysis_criterias', serializer_type: AnalysisCriteria::CANONICAL_SERIALIZER } }
      let(:serializer_factory_instance) do
        serializer_factory.new(analysis_criterias: analysis_criterias, params: params)
      end
      let(:options) { { include: [:rule], is_collection: true } }
      let(:serializer) { V1::AnalysisCriteriaSerializers::AnalysisCriteriaSerializer }

      it 'builds correct serializer' do
        expect(serializer).to receive(:new).with(analysis_criterias, options)
        serializer_factory_instance.build_serializer
      end

      context 'and incorrect version route' do
        let(:params) { { controller: '/analysis_criterias', serializer_type: AnalysisCriteria::CANONICAL_SERIALIZER } }
        let(:serializer_factory_instance) do
          serializer_factory.new(analysis_criterias: analysis_criterias, params: params)
        end

        it 'returns Failure' do
          expect(serializer_factory_instance).to receive(:serializer_api_version).with(
            controller_route: '/analysis_criterias'
          ).and_return(Failure(Error::ServerError::IncorrectApiVersion))
          serializer_factory_instance.build_serializer
        end
      end
    end
  end
end
