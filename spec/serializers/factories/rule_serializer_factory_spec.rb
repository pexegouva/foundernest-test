# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Factories::RuleSerializerFactory, type: :factory do
  subject(:serializer_factory) { Factories::RuleSerializerFactory }

  describe '#build_serializer' do
    let(:rules) { build_list(:rule, 2) }

    context 'when incorrect serializer type' do
      let(:params) { {} }
      let(:serializer_factory_instance) { serializer_factory.new(rules: rules, params: params) }

      it 'returns Failure SerializerTypeNotSelectedError' do
        response = serializer_factory_instance.build_serializer
        expect(response).to eq Failure(Error::ServerError::SerializerTypeNotSelected)
      end
    end

    context 'when serializer CANONICAL_SERIALIZER type defined and correct version route' do
      let(:params) { { controller: 'v1/rules', serializer_type: Rule::CANONICAL_SERIALIZER } }
      let(:serializer_factory_instance) { serializer_factory.new(rules: rules, params: params) }
      let(:options) { { is_collection: true } }
      let(:serializer) { V1::RuleSerializers::RuleSerializer }

      it 'builds correct serializer' do
        expect(serializer).to receive(:new).with(rules, options)
        serializer_factory_instance.build_serializer
      end

      context 'and incorrect version route' do
        let(:params) { { controller: '/rules', serializer_type: Rule::CANONICAL_SERIALIZER } }
        let(:serializer_factory_instance) { serializer_factory.new(rules: rules, params: params) }

        it 'returns Failure' do
          expect(serializer_factory_instance).to receive(:serializer_api_version).with(
            controller_route: '/rules'
          ).and_return(Failure(Error::ServerError::IncorrectApiVersion))
          serializer_factory_instance.build_serializer
        end
      end
    end
  end
end
