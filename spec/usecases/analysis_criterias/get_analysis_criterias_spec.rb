# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AnalysisCriterias::GetAnalysisCriterias, type: :usecase do
  subject(:use_case) { AnalysisCriterias::GetAnalysisCriterias }

  describe '#perform' do
    let(:company) { build(:company) }
    let(:use_case_instance) { use_case.new(company_id: company.identity) }

    context 'when find_analysis_criterias contains exception' do
      before do
        allow(use_case_instance).to receive(:find_analysis_criterias).and_return(Error(StandardError))
      end

      it 'returns Failure' do
        result = use_case_instance.perform
        expect(result).to eq Failure(StandardError)
      end
    end
  end
end
