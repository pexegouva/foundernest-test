# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Rules::GetUserRules, type: :usecase do
  subject(:use_case) { Rules::GetUserRules }

  describe '#perform' do
    let(:investor) { build(:investor) }
    let(:use_case_instance) { use_case.new(user: investor) }

    context 'when find_user_rules contains exception' do
      before do
        allow(use_case_instance).to receive(:find_user_rules).and_return(Error(StandardError))
      end

      it 'returns Failure' do
        result = use_case_instance.perform
        expect(result).to eq Failure(StandardError)
      end
    end
  end
end
