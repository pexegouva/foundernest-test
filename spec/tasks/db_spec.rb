# frozen_string_literal: true

require 'rails_helper'

Rails.application.load_tasks

RSpec.describe 'db task', type: :task do
  context 'when executing populate task' do
    it 'generates documents correctly' do
      run_task('db:populate')

      expect(User.count).to eq 1
      expect(Rule.count).to eq 15
      expect(Company.count).to eq 10
      expect(AnalysisCriteria.count).to eq 150

      companies = Company.all
      companies.each do |company|
        expect(company.analysis).not_to eq nil
      end
    end
  end

  context 'when executing reset task' do
    before do
      run_task('db:populate')
    end

    it 'removes generated documents correctly' do
      run_task('db:reset')

      expect(User.count).to eq 0
      expect(Rule.count).to eq 0
      expect(Company.count).to eq 0
      expect(AnalysisCriteria.count).to eq 0
    end
  end
end

def run_task(task_name)
  Rake::Task[task_name].invoke
end
