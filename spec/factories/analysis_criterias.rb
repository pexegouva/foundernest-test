# frozen_string_literal: true

FactoryBot.define do
  factory :analysis_criteria do
    factory :analysis_criteria_with_rule do
      association :rule, factory: :rule_with_user, strategy: :create
    end

    factory :analysis_criteria_with_random_match_analysis do
      match_analysis { AnalysisCriteria::MATCH_ANALYSIS_STATES.sample }
    end
  end
end
