# frozen_string_literal: true

FactoryBot.define do
  factory :investor do
    full_name { Faker::Games::Fallout.character }
    username { Faker::Internet.unique.username(specifier: 2..60) }
    email { Faker::Internet.unique.email }

    factory :brotherhood_of_steel_investor do
      full_name { 'Knight Meinar' }
      username { 'beautiful_knight' }
      email { 'knight_meinar@brotherhoodofsteel.com' }
    end
  end
end
