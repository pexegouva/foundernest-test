# frozen_string_literal: true

FactoryBot.define do
  factory :company do
    name { Faker::Games::Zelda.unique.character }
    site_url { Faker::Internet.url }

    factory :company_with_random_status do
      status { Company::DECISION_STATES.sample }
    end

    factory :company_with_analysis do
      association :analysis, factory: :analysis, strategy: :build
    end
  end
end
