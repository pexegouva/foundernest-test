# frozen_string_literal: true

FactoryBot.define do
  factory :analysis do
    matching_score { rand(0..100) }
    warnings { rand(0..100) }
    missing_information { rand(0..100) }
    must_haves { Analysis::MUST_HAVE_STATES.sample }
    nice_to_haves { rand(0..100) }
    super_nice_to_haves { rand(0..100) }
  end
end
