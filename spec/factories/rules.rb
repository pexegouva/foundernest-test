# frozen_string_literal: true

FactoryBot.define do
  factory :rule do
    definition { Faker::Movies::Hobbit.quote }

    factory :rule_with_user do
      association :user, factory: :user, strategy: :build
    end

    factory :rule_with_random_importance do
      importance { Rule::IMPORTANCE_LEVELS.sample }
    end
  end
end
