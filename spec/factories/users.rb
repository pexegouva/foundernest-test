# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    full_name { Faker::Games::Fallout.character }
    username { Faker::Internet.unique.username(specifier: 2..60) }
    email { Faker::Internet.unique.email }
  end
end
