# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CompanyPolicy, type: :policy do
  subject(:policy) { CompanyPolicy }

  describe '#owner?' do
    let(:investor) { build(:investor) }
    let(:company) { build(:company) }

    context 'when company exists' do
      let(:policy_instance) { policy.new(current_user: investor, resource: company) }

      context 'and user is owner' do
        before do
          company.investor = investor
        end

        it 'returns true' do
          is_owner = policy_instance.owner?
          expect(is_owner).to eq true
        end
      end

      context 'and user is not owner' do
        it 'returns false' do
          is_owner = policy_instance.owner?
          expect(is_owner).to eq false
        end
      end
    end

    context 'when company does not exists' do
      let(:policy_instance) { policy.new(current_user: investor, resource: nil) }

      it 'returns false' do
        is_owner = policy_instance.owner?
        expect(is_owner).to eq false
      end
    end
  end
end
