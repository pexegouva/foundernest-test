# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'AnalysisCriteria routes', type: :routing do
  describe 'GET v1/company/:id/analysis_criterias' do
    it 'routes to analysis_criterias#index' do
      expect(get: 'v1/companies/:id/analysis_criterias').to route_to(
        format: 'json',
        controller: 'v1/analysis_criterias',
        action: 'index',
        id: ':id'
      )
    end
  end
end
