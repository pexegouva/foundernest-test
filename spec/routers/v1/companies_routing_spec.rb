# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Company routes', type: :routing do
  describe 'GET v1/companies' do
    it 'routes to companies#index' do
      expect(get: 'v1/companies').to route_to(
        format: 'json',
        controller: 'v1/companies',
        action: 'index'
      )
    end
  end

  describe 'PATCH v1/companies/:id' do
    it 'routes to companies#index' do
      expect(patch: 'v1/companies/:id').to route_to(
        format: 'json',
        controller: 'v1/companies',
        action: 'update',
        id: ':id'
      )
    end
  end
end
