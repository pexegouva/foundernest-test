# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Rules routes', type: :routing do
  describe 'GET v1/rules' do
    it 'routes to rules#index' do
      expect(get: 'v1/rules').to route_to(
        format: 'json',
        controller: 'v1/rules',
        action: 'index'
      )
    end
  end
end
