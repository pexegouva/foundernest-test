# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /v1/rules', type: :request do
  describe 'Get rules from current user' do
    context 'when current_user do not exists' do
      it 'returns Error::NotFound::UserNotFound' do
        get v1_rules_path
        expect(response.status).to eq 404

        response_body = json_body['error']
        expect(response_body['status']).to eq 404
        expect(response_body['error']).to eq 'not_found'
        expect(response_body['code']).to eq USER_NOT_FOUND_ERROR_CODE
        expect(response_body['message']).to eq 'User was not found in DB'
      end
    end

    context 'when current_user exists' do
      let!(:investor) { create(:investor) }

      context 'but that user do not have rules' do
        it 'returns no data' do
          get v1_rules_path
          expect(response.status).to eq 200

          response_body = json_body['data']
          expect(response_body).to eq []
        end
      end

      context 'and that user has rules' do
        let(:another_investor) { create(:investor) }
        let!(:rules) { build_list(:rule, 3) }

        before do
          rules.first.user = investor
          rules[1].user = investor
          rules[2].user = another_investor

          rules.first.save!
          rules[1].save!
          rules[2].save!

          get v1_rules_path
        end

        it 'returns that user rules' do
          expect(response.status).to eq 200

          response_body = json_body['data']
          expect(response_body.length).to eq 2

          first_item = response_body.first
          second_item = response_body[1]
          expect(first_item['id']).to eq rules.first.identity
          expect(second_item['id']).to eq rules[1].identity
        end

        # it 'serializes data correctly' do
        #   response_body = json_body['data']
        #   first_item = response_body.first

        #   expect(first_item['id']).to eq rules.first.identity
        #   expect(first_item['type']).to eq 'user_criteria'

        #   rule_relationships = first_item['relationships']['rule']['data']
        #   expect(rule_relationships['id']).to eq rules.first.rule.identity
        #   expect(rule_relationships['type']).to eq 'rule'

        #   first_included_data = json_body['included'].first
        #   expect(first_included_data['id']).to eq rules.first.identity
        #   expect(first_included_data['attributes']['definition']).to eq rules.first.definition
        #   expect(first_included_data['attributes']['importance']).to eq rules.first.importance
        # end
      end
    end
  end
end
