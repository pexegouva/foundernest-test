# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /v1/companies/:id/analysis_criterias', type: :request do
  describe 'Get analysis criterias for a given company' do
    context 'when current_user do not exists' do
      it 'returns Error::NotFound::UserNotFound' do
        get analysis_criterias_v1_company_path(id: 'fake_id')
        expect(response.status).to eq 404

        response_body = json_body['error']
        expect(response_body['status']).to eq 404
        expect(response_body['error']).to eq 'not_found'
        expect(response_body['code']).to eq USER_NOT_FOUND_ERROR_CODE
        expect(response_body['message']).to eq 'User was not found in DB'
      end
    end

    context 'when current_user exists' do
      let!(:investor) { create(:investor) }
      let(:company) { create(:company, investor: investor) }

      context 'but that user do not have company with analysis' do
        it 'returns no data' do
          get analysis_criterias_v1_company_path(id: 'fake_analysis')
          expect(response.status).to eq 200

          response_body = json_body['data']
          expect(response_body).to eq []
        end
      end

      context 'and that user has companies with analysis' do
        let(:another_investor) { create(:investor) }
        let(:another_company) { create(:company, investor: another_investor) }

        let!(:analysis_criterias) { build_list(:analysis_criteria_with_rule, 3) }

        before do
          analysis_criterias.first.company = company
          analysis_criterias[1].company = company
          analysis_criterias[2].company = another_company

          analysis_criterias.first.save!
          analysis_criterias[1].save!
          analysis_criterias[2].save!

          get analysis_criterias_v1_company_path(id: company.identity)
        end

        it 'returns that analysis criterias' do
          expect(response.status).to eq 200

          response_body = json_body['data']
          expect(response_body.length).to eq 2

          first_item = response_body.first
          second_item = response_body[1]
          expect(first_item['id']).to eq analysis_criterias.first.identity
          expect(second_item['id']).to eq analysis_criterias[1].identity
        end

        it 'serializes data correctly' do
          response_body = json_body['data']
          first_item = response_body.first

          expect(first_item['id']).to eq analysis_criterias.first.identity
          expect(first_item['type']).to eq 'analysis_criteria'

          analysis_criteria_attributes = first_item['attributes']
          expect(analysis_criteria_attributes['match_analysis']).to eq 'unknown'

          rule_relationships = first_item['relationships']['rule']['data']
          expect(rule_relationships['id']).to eq analysis_criterias.first.rule.identity
          expect(rule_relationships['type']).to eq 'rule'

          first_included_data = json_body['included'].first
          expect(first_included_data['id']).to eq analysis_criterias.first.rule.identity
          expect(first_included_data['attributes']['definition']).to eq analysis_criterias.first.rule.definition
          expect(first_included_data['attributes']['importance']).to eq analysis_criterias.first.rule.importance
        end
      end
    end
  end
end
