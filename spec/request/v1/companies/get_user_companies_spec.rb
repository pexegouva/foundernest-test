# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /v1/companies', type: :request do
  describe 'Get companies from current user' do
    context 'when current_user do not exists' do
      it 'returns Error::NotFound::UserNotFound' do
        get v1_companies_path
        expect(response.status).to eq 404

        response_body = json_body['error']
        expect(response_body['status']).to eq 404
        expect(response_body['error']).to eq 'not_found'
        expect(response_body['code']).to eq USER_NOT_FOUND_ERROR_CODE
        expect(response_body['message']).to eq 'User was not found in DB'
      end
    end

    context 'when current_user exists' do
      let!(:investor) { create(:investor) }

      context 'but that user do not have companies' do
        it 'returns no data' do
          get v1_companies_path
          expect(response.status).to eq 200

          response_body = json_body['data']
          expect(response_body).to eq []
        end
      end

      context 'and that user has companies' do
        let(:another_investor) { create(:investor) }
        let!(:companies) { build_list(:company_with_analysis, 3) }

        before do
          companies.first.investor = investor
          companies[1].investor = investor
          companies[2].investor = another_investor

          companies.first.save!
          companies[1].save!
          companies[2].save!

          get v1_companies_path
        end

        it 'returns that user companies' do
          expect(response.status).to eq 200

          response_body = json_body['data']
          expect(response_body.length).to eq 2

          first_item = response_body.first
          second_item = response_body[1]
          expect(first_item['id']).to eq companies.first.identity
          expect(second_item['id']).to eq companies[1].identity
        end

        it 'serializes data correctly' do
          response_body = json_body['data']
          first_item = response_body.first

          expect(first_item['id']).to eq companies.first.identity
          expect(first_item['type']).to eq 'company'

          company_attributes = first_item['attributes']
          expect(company_attributes['name']).to eq companies.first.name
          expect(company_attributes['site_url']).to eq companies.first.site_url
          expect(company_attributes['status']).to eq companies.first.status

          analysis_data = company_attributes['analysis']
          expect(analysis_data['matching_score']).to eq companies.first.analysis.matching_score
          expect(analysis_data['warnings']).to eq companies.first.analysis.warnings
          expect(analysis_data['missing_info']).to eq companies.first.analysis.missing_information
          expect(analysis_data['must_haves']).to eq companies.first.analysis.must_haves
          expect(analysis_data['super_nice_to_haves']).to eq companies.first.analysis.super_nice_to_haves
          expect(analysis_data['nice_to_haves']).to eq companies.first.analysis.nice_to_haves
        end
      end
    end
  end
end
