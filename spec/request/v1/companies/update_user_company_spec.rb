# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'PATCH /v1/companies/:id', type: :request do
  describe 'Updates given company' do
    context 'when current_user do not exists' do
      it 'returns Error::NotFound::UserNotFound' do
        patch v1_company_path(id: 'fake_id')
        expect(response.status).to eq 404

        response_body = json_body['error']
        expect(response_body['status']).to eq 404
        expect(response_body['error']).to eq 'not_found'
        expect(response_body['code']).to eq USER_NOT_FOUND_ERROR_CODE
        expect(response_body['message']).to eq 'User was not found in DB'
      end
    end

    context 'when current_user exists' do
      let!(:investor) { create(:investor) }

      context 'and company param missing' do
        let(:params) { { status: 'fake_status' } }

        it 'returns ActionController::ParameterMissing' do
          patch v1_company_path(id: 'fake_id'), params: params
          expect(response.status).to eq 400

          response_body = json_body['error']
          expect(response_body['status']).to eq 400
          expect(response_body['error']).to eq 'bad_request'
          expect(response_body['code']).to eq PARAMETER_MISSING_ERROR_CODE
          expect(response_body['message']).to eq 'param is missing or the value is empty: company'
        end
      end

      context 'and company param present' do
        let(:company) { build(:company) }

        context 'when company exists' do
          let(:params) { { company: { status: 'discarded' } } }

          before do
            company.investor = investor
            company.save!
          end

          it 'updates company status' do
            patch v1_company_path(id: company.identity), params: params
            expect(response.status).to eq 204

            updated_company = Company.find(company.id)
            expect(updated_company.status).to eq 'discarded'
          end

          context 'but status is incorrect' do
            let(:params) { { company: { status: 'fake_status' } } }

            it 'returns Error::UnprocessableEntity::MongoidValidation' do
              patch v1_company_path(id: company.identity), params: params
              expect(response.status).to eq 422

              response_body = json_body['error']
              expect(response_body['status']).to eq 422
              expect(response_body['error']).to eq 'unprocessable_entity'
              expect(response_body['code']).to eq MONGOID_VALIDATION_ERROR_CODE
            end
          end
        end

        context 'and company do not exists' do
          let(:params) { { company: { status: 'fake_status' } } }

          it 'returns Error::NotFound::CompanyNotFound' do
            patch v1_company_path(id: 'fake_id'), params: params
            expect(response.status).to eq 404

            response_body = json_body['error']
            expect(response_body['status']).to eq 404
            expect(response_body['error']).to eq 'not_found'
            expect(response_body['code']).to eq COMPANY_NOT_FOUND_ERROR_CODE
            expect(response_body['message']).to eq 'Company was not found in DB'
          end
        end
      end
    end
  end
end
