# frozen_string_literal: true

require 'factory_bot'

namespace :db do
  desc 'Populate Database to be able to check endpoints'
  task populate: :environment do
    include FactoryBot::Syntax::Methods

    investor = create(:brotherhood_of_steel_investor)
    puts 'Investor created'

    rules = build_list(:rule_with_random_importance, 15)
    rules.each do |rule|
      rule.user = investor
      rule.save!
    end
    puts 'Rules created for the given investor'

    companies = create_list(:company_with_random_status, 10, investor: investor)
    puts 'Companies created for the given investor'

    analysis = build_list(:analysis, 10)
    analysis.each_with_index do |individual_analysis, index|
      individual_analysis.company = companies[index]
      individual_analysis.save!
    end
    puts 'Analysis created for the given investor'

    companies.each do |company|
      rules.each do |rule|
        create(:analysis_criteria_with_random_match_analysis, company: company, rule: rule)
      end
    end
    puts 'Analysis criterias created'
  end

  task reset: :environment do
    User.delete_all
    Company.delete_all
    Rule.delete_all
    AnalysisCriteria.delete_all
  end
end
