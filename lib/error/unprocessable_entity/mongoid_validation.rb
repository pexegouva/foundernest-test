# frozen_string_literal: true

module Error::UnprocessableEntity
  class MongoidValidation < UnprocessableEntity
    def initialize(message: nil)
      super(message: message || 'MongoID validation error', code: MONGOID_VALIDATION_ERROR_CODE)
    end
  end
end
