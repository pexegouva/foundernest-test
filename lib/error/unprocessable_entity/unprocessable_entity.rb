# frozen_string_literal: true

module Error::UnprocessableEntity
  class UnprocessableEntity < StandardError
    attr_reader :status, :error, :message, :code

    def initialize(message:, code:)
      @error = :unprocessable_entity
      @status = 422
      @code = code || 0
      @message = message || 'Server was unable to process given information'
    end
  end
end
