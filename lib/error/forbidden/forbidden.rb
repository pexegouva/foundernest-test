# frozen_string_literal: true

module Error::Forbidden
  class Forbidden < StandardError
    attr_reader :status, :error, :message, :code

    def initialize(message:, code:)
      @error = :forbidden
      @status = 403
      @code = code || 0
      @message = message || 'Access to given resource is forbidden'
    end
  end
end
