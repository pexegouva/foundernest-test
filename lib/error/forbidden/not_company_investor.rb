# frozen_string_literal: true

module Error::Forbidden
  class NotCompanyInvestor < Forbidden
    def initialize
      super(
        message: 'User is not the investor of the given Company',
        code: NOT_COMPANY_INVESTOR_ERROR_CODE
      )
    end
  end
end
