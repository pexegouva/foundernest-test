# frozen_string_literal: true

module Error::NotFound
  class UserNotFound < NotFound
    def initialize
      super(message: 'User was not found in DB', code: USER_NOT_FOUND_ERROR_CODE)
    end
  end
end
