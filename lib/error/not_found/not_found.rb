# frozen_string_literal: true

module Error::NotFound
  class NotFound < StandardError
    attr_reader :status, :error, :message, :code

    def initialize(message:, code:)
      @error = :not_found
      @status = 404
      @code = code || 0
      @message = message || 'Resource not found'
    end
  end
end
