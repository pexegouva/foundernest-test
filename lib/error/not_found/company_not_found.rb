# frozen_string_literal: true

module Error::NotFound
  class CompanyNotFound < NotFound
    def initialize
      super(message: 'Company was not found in DB', code: COMPANY_NOT_FOUND_ERROR_CODE)
    end
  end
end
