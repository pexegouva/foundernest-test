# frozen_string_literal: true

module Error
  module ErrorHandler
    def self.included(clazz)
      clazz.class_eval do
        rescue_from NotFound::NotFound do |e|
          respond(e.error, e.status, e.message, e.code)
        end
        rescue_from ServerError::InternalServerError do |e|
          respond(e.error, e.status, e.message, e.code)
        end
        rescue_from UnprocessableEntity::UnprocessableEntity do |e|
          respond(e.error, e.status, e.message, e.code)
        end
        rescue_from Forbidden::Forbidden do |e|
          respond(e.error, e.status, e.message, e.code)
        end
        rescue_from ActionController::ParameterMissing do |e|
          respond(:bad_request, 400, e.message, PARAMETER_MISSING_ERROR_CODE)
        end
      end
    end

    private

    def respond(error, status, message, code)
      json = Helpers::Render.json(status: status, code: code, error: error, message: message)
      render json: json, status: status
    end
  end
end
