# frozen_string_literal: true

module Error::ServerError
  class SerializerTypeNotSelected < InternalServerError
    def initialize
      super(message: 'Serializer type not selected, use existing one', code: SERIALIZER_TYPE_NOT_SELECTED_ERROR_CODE)
    end
  end
end
