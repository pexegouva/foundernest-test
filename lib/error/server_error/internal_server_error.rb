# frozen_string_literal: true

module Error::ServerError
  class InternalServerError < StandardError
    attr_reader :status, :error, :message, :code

    def initialize(message:, code:)
      @error = :internal_server_error
      @status = 500
      @code = code || 0
      @message = message || 'Unexpected error'
    end
  end
end
