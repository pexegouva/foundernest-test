# frozen_string_literal: true

module Error::ServerError
  class IncorrectApiVersion < InternalServerError
    def initialize
      super(message: 'There is no version in the given route', code: INCORRECT_API_VERSION_ERROR_CODE)
    end
  end
end
