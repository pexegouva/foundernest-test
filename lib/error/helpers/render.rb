# frozen_string_literal: true

module Error::Helpers
  class Render
    def self.json(status:, code:, error:, message:)
      {
        error: {
          status: status,
          code: code,
          error: error,
          message: message
        }
      }.as_json
    end
  end
end
